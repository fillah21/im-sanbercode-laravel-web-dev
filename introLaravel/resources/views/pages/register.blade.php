<!DOCTYPE html>
<html>
  <head>
    <title>Form Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
      @csrf
      <label>First name :</label><br /><br />
      <input type="text" name="firstName" /><br /><br />

      <label>Last name :</label><br /><br />
      <input type="text" name="lastName" /><br /><br />

      <label>Gender :</label><br /><br />
      <input type="radio" name="gender" />Male <br />
      <input type="radio" name="gender" />Female <br />
      <input type="radio" name="gender" />Other <br /><br />

      <label>Nationality :</label><br /><br />
      <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option></select
      ><br /><br />

      <label>Language Spoken</label><br /><br />
      <input type="checkbox" name="bahasa" />Bahasa Indonesia <br />
      <input type="checkbox" name="bahasa" />English <br />
      <input type="checkbox" name="bahasa" />Other <br /><br />

      <label>Bio</label><br /><br />
      <textarea name="bio" cols="30" rows="10" name="bio"></textarea><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
