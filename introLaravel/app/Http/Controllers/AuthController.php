<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('pages.register');
    }

    public function welcome(Request $request) {
        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');

        return view('pages.welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
