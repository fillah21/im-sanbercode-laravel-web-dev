<?php 
    require_once("animal.php");

    class Frog extends Animal {
        public $jump = "hop-hop";

        public function jump() {
            echo "Name : " . $this->name . "<br>"; 
            echo "legs : " . $this->legs . "<br>"; 
            echo "cold blooded : " . $this->cold_blooded . "<br>";
            echo "jump : " . $this->jump . "<br><br>";
        }
    }
?>