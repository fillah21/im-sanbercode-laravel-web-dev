@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('subtitle')
    Detail Cast
@endsection

@section('content')
    <h1>{{ $cast->nama }} ({{ $cast->umur }} tahun)</h1>
    <p>{{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection