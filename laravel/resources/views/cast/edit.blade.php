@extends('layouts.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('subtitle')
    Edit Cast
@endsection

@section('content')
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" value="{{ $cast->nama }}" id="nama" name="nama">
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>


        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" value="{{ $cast->umur }}" id="umur" name="umur">
            @error('umur')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control @error('bio') is-invalid @enderror" id="bio" rows="3" name="bio">{{ $cast->bio }}</textarea>
            @error('bio')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection