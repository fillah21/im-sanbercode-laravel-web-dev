@extends('layouts.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('subtitle')
    Tambah Cast
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama">
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>


        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur">
            @error('umur')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control @error('bio') is-invalid @enderror" id="bio" rows="3" name="bio"></textarea>
            @error('bio')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection