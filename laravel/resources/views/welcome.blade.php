@extends('layouts.master')

@section('title')
    Halaman Utama
@endsection

@section('subtitle')
    Utama
@endsection

@section('content')
    <ul>
        <li><a href="/table">Table</a></li>
        <li><a href="/data-table">Data Table</a></li>
    </ul>
@endsection