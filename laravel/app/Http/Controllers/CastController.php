<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // Validation
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required|numeric',
            'bio' => 'required'
        ]);

        // Insert to database
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($cast_id)
    {
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($cast_id, Request $request)
    {
        // Validation
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required|numeric',
            'bio' => 'required'
        ]);

        // Update to database
        DB::table('cast')
              ->where('id', $cast_id)
              ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio'),
                ]
            );

        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        DB::table('cast')->where('id', $cast_id)->delete();

        return redirect('/cast');
    }
}
